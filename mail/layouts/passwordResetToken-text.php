<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 10.07.17
 * Time: 13:01
 */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

Hello <?= $user->username ?>,
Follow the link below to reset your password:

<?= $resetLink ?>