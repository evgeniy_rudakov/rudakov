<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 23.07.17
 * Time: 14:33
 */

namespace app\components;
use Yii;
use app\models\Content;
use yii\base\Widget;
use yii\helpers\Html;

class renderContentView extends Widget
{
    public $model;
    private $html;

    public function init()
    {
        parent::init();
        if (isset($this->model)) {
            $model = $this->model;
            $this->html = '<div class="col-md-3 col-sm-6 col-padding animate-box" data-animate-effect="fadeInLeft">' .
                '<div class="blog-entry">' .
                Html::a(Html::img($model->getImgUrl('small', '1'), ['class' => 'img-responsive', 'alt' => $model->title]),
                    ['/' . $model->category->url . '/view', 'id' => $model->id],
                    ['class' => 'blog-img']) .
                '<div class="desc">' .
                    '<h3>' .
                    Html::a($model->title,
                        ['/' . $model->category->url . '/view', 'id' => $model->id],
                        []
                    ) .
                    '</h3>' .
                '<span><small>' . $model->user->username . '</small> / <small>' . $model->category->name . '</small> /
                 <small> <i class="icon-comment"></i> '.Yii::$app->formatter->asDate($model->created_at).'</small></span>' .
                '<p>' . $model->cat . '</p>' .
                Html::a('Больше<i class="icon-arrow-right3"></i></a>',
                    ['/' . $model->category->url . '/view', 'id' => $model->id],
                    ['class' => 'lead']) .
                '</div>' .
                '</div>' .
                '</div>';
        } else {
            $this->html = '';
        }

    }

    public function run()
    {
        return Html::decode($this->html);
    }
}