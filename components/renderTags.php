<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 23.07.17
 * Time: 23:50
 */


namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class renderTags extends Widget

{
    public $model;
    private $result_tags_string;

    public function init()
    {
        parent::init();
        $model = $this->model;
        if (!empty($model->itemsTags)) {
            $items = '';
            foreach ($model->itemsTags as $itemTag) {
                $items .= '<li>' . Html::a($itemTag->tag->text,'js:',['class'=>'tag']) . '</li>';
            }
            $this->result_tags_string = '<div class="tags"><ul class="tags-list">' . $items . '</ul></div>';
        } else {
            $this->result_tags_string = '';
        }
    }

    public
    function run()
    {
        return Html::decode($this->result_tags_string);
    }
}
