<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';


    public function init()
    {
        parent::init();


//        $this->baseUrl = Yii::$app->view->theme->baseUrl;

        if (explode('/', $_SERVER['REQUEST_URI'])[1] == 'admin') {
            $this->css = [
                'css/admin/site.css',
                'css/admin/jquery.tagsinput.css',
            ];
            $this->js = [
                'js/admin/jquery.autocomplete.min.js',
                'js/admin/jquery.tagsinput.js',
                'js/admin/script.js',
            ];

        } else {

            $this->css = [
                'css/animate.css',
                'css/icomoon.css',
                'css/bootstrap.css',
                'css/flexslider.css',
                'css/style.css',
                'css/dev_style.css',
            ];
            $this->js = [
                'js/jquery.easing.1.3.js',
                'js/bootstrap.min.js',
                'js/jquery.waypoints.min.js',
                'js/jquery.flexslider-min.js',
                'js/main.js'

            ];
        }
        $this->depends = ['yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',];
//        $this->jsOptions = ['position' => \yii\web\View::POS_READY];
        // Dev
    }
}
