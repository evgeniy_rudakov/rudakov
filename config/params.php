<?php

return [
    'adminEmail' => 'evgeny@rudakov.site',
    'user.passwordResetTokenExpire' => 3600,
    'supportEmail' => 'evgeny@rudakov.site'
];
