<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use app\modules\admin\models\LoginForm;
use yii\filters\AccessControl;

/**
 * Default controller for the `admin` module
 */
class SiteController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function behaviors()
{
    return [
//        'access' => [
//            'class' => AccessControl::className(),
//            'rules' => [
//                [
//                    'allow' => true,
//                    'actions'=>['login','error'],
//                    'roles' => ['?'],
//                ],
//                [
//                    'allow' => true,
//                    'actions'=>['index'],
//                    'roles' => ['admin','salon_director'],
//                ],
//            ],
//        ],
    ];
}

    public function actionIndex() {
        return $this->render('index');
    }
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect("/admin");
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect("/admin");
        }
        return $this->render('login', ['model' => $model]);
    }


    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->redirect("/admin");
    }

}
