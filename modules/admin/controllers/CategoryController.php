<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;


/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Category models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		return $this->render('index', [
		]);
	}

	public function actionUrlChange()
	{

		if (isset($_POST['Category']['url']))
		{
			$save_errors = [];
			foreach ($_POST['Category']['url'] as $id => $url_value)
			{
				$category      = Category::findOne($id);
				$category->url = $url_value;
				if (!$category->save())
				{
					$save_errors[$id] = $category->errors;
				}
			}
			if (empty($save_errors))
			{
				Yii::$app->getSession()->addFlash('success', 'Save');
			}
			else
			{
				Yii::$app->getSession()->addFlash('error', 'There are errors');
			}
		}
		$categories = Category::find()->where(['user_id' => Yii::$app->user->identity->id])->all();

		return $this->render('url-change', [
			'categories' => $categories,
		]);
	}


	protected function findModel($id)
	{
		if (($model = Category::findOne($id)) !== null)
		{
			return $model;
		}
		else
		{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
