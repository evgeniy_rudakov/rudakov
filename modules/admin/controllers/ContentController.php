<?php

namespace app\modules\admin\controllers;

use app\models\Content;
use app\models\Tags;
use app\modules\admin\models\ContentSearch;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\filters\VerbFilter;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ContentController implements the CRUD actions for Content model.
 */
class ContentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Content model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Content model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Content();
        $treeViewValue = '';
        $user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = $user->id;
            $model->created_at = time();
            $treeViewValue = $model->category_id;
            if ($model->save()) {
                $this->imageSave($model);
                $model->imageFile = null;
                Tags::updateTags($model->id, Tags::CONTENT_TAG_TYPE, $_POST['tags']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'treeViewValue' => $treeViewValue,
            'user' => $user,
            'tags' => '',
        ]);
    }

    /**
     * Updates an existing Content model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $treeViewValue = $model->category_id;
        $user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post())) {
            $treeViewValue = $model->category_id;
            if ($model->save()) {
                $this->imageSave($model);
                $model->imageFile = null;
                Tags::updateTags($model->id, Tags::CONTENT_TAG_TYPE, $_POST['tags']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'treeViewValue' => $treeViewValue,
            'user' => $user,
            'tags' => Tags::loadTags($model->id, Tags::CONTENT_TAG_TYPE)
        ]);
    }

    /**
     * Deletes an existing Content model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->deleteImage($model);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Save image from _from
     * @param $model
     *
     */
    private function imageSave($model)
    {
        $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
        if ($model->upload()) {
            if (file_exists(Yii::getAlias('@webroot/img/content/' . $model->id . '/1/orig.jpg'))) {
                Image::thumbnail('@webroot/img/content/' . $model->id . '/1/orig.jpg', 256, 205, ManipulatorInterface::THUMBNAIL_INSET)
                    ->save(Yii::getAlias('@webroot/img/content/' . $model->id . '/1/mini.jpg'), ['quality' => 100]);
                Image::thumbnail('@webroot/img/content/' . $model->id . '/1/orig.jpg', 500, 400, ManipulatorInterface::THUMBNAIL_INSET)
                    ->save(Yii::getAlias('@webroot/img/content/' . $model->id . '/1/small.jpg'), ['quality' => 100]);
            }
        }
    }

    /**
     * Delete image from web/img/$model->id
     * @param $model
     */


    private function deleteImage($model)
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/img/content/1/';
        foreach (['orig', 'mini', 'small'] as $size) {
            $file = $path . $model->id . '/' . $size . '.jpg';
            if (file_exists($file)) {
                unlink($file);
            }
        }
        if (is_dir($path . '/' . $model->id)) {
            rmdir($path . '/' . $model->id);
        }
    }

    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Content the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
