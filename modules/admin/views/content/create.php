<?php


/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = Yii::t('app', 'Create Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Portfolios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-create">


    <?= $this->render('_form', [
        'model' => $model,
        'treeViewValue' => $treeViewValue,
        'user' => $user,
        'tags' => $tags,
    ]) ?>

</div>
