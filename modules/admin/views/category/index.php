<?php

use yii\helpers\Html;
use kartik\tree\TreeView;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Категории');
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->identity;
?>
<div class="category-index">
<?php
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => Category::find()->where(['user_id'=>$user->id])->addOrderBy('root, lft'),
    'headingOptions' => ['label' => 'Категории услуг'],
    'fontAwesome' => false,     // optional
    'isAdmin' => true,         // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => false,       // defaults to true
    'cacheSettings' => [
        'enableCache' => true   // defaults to true
    ]
]);

$send_form = <<<JS
$('.kv-tree-container').on('click',function() {
  var focus_elem = $('.kv-focussed');
  var focus_li = $(focus_elem).parent().parent();
  var focus_lvl = $(focus_li).attr("data-lvl");
  var focus_id = $(focus_li).attr("data-key");
  if (focus_lvl==1){
      $('.kv-create').prop('disabled', true);
  }
});
JS;
$this->registerJs($send_form, yii\web\View::POS_READY);

?>
</div>
