<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 29.07.17
 * Time: 14:35
 */

?>
<?php

use yii\helpers\Html;
use kartik\tree\TreeView;
use app\models\Category;
$this->title = Yii::t('app', 'Категории');
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->identity;
?>
<div class="category-index">
<?php
\yii\widgets\Pjax::begin();
$form = \yii\widgets\ActiveForm::begin([
	'method'=>'post'
]);

foreach ($categories as  $i=>$category){
	echo $form->field($category,'url['.$category->id.']')->textInput(['value'=>$category->url])->label($category->name);
}
echo Html::submitButton('Save',['class'=>'btn btn-success']);

\yii\widgets\ActiveForm::end();
\yii\widgets\Pjax::end();
?>
</div>

