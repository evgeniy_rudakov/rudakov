<aside class="main-sidebar">

    <section class="sidebar">


        <?=
        dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Пользователи', 'icon' => 'fa fa-table', 'url' => ['/admin/user']],
                    ['label' => 'Содержание', 'options' => ['class' => 'header']],
                    ['label' => 'Категории', 'icon' => 'fa fa-table', 'url' => ['/admin/category']],
                    ['label' => 'URL категорий', 'icon' => 'fa fa-table', 'url' => ['/admin/category/url-change']],
                    ['label' => 'Контент', 'icon' => 'fa fa-table', 'url' => ['/admin/content']],
                    ['label' => 'Теги', 'icon' => 'fa fa-table', 'url' => ['/admin/tags']],
                    ['label' => 'Контакты', 'icon' => 'fa fa-table', 'url' => ['/admin/contact']],
                ],
            ]
        )
        ?>

    </section>

</aside>
