<?php

use app\models\User;
?>

<?php $this->beginBlock('content-header'); ?>
Информация
<?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Пользователей</span>
                <span class="info-box-number"><?= User::find()->count() ?></span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>
</div>
