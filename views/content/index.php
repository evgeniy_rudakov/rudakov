<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 12.07.17
 * Time: 23:02
 */
use yii\helpers\Html;
use app\components\renderContentView;
?>
<div id="fh5co-page">
    <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
    <?= $this->context->renderPartial('/layouts/menu',['page_url'=>$page_url]) ?>


    <div id="fh5co-main">
        <div class="fh5co-narrow-content">
            <h2 class="fh5co-heading animate-box" data-animate-effect="fadeInLeft"><?=$category->name?></h2>
            <div class="row row-bottom-padded-md">
                <?php
                foreach ($category->content as $content){
                echo renderContentView::widget(['model'=>$content]);
                }?>
            </div>
        </div>
    </div>
</div>
