<?php

use app\components\renderTags;

?>

<div id="fh5co-page">
    <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>

	<?= $this->context->renderPartial('/layouts/menu', ['page_url' => $page_url]) ?>


    <div id="fh5co-main">
        <div class="fh5co-narrow-content">
            <div class="row row-bottom-padded-md">
                <div class="col-md-11 animate-box" data-animate-effect="fadeInLeft">
                    <h2 class="fh5co-heading"><?= $model->title ?></h2>
					<?= $model->content ?>
					<?= renderTags::widget(['model' => $model]) ?>
					<?= \yii\helpers\Html::a('Назад', ['/' . $model->category->url], ['class' => 'btn btn-primary']) ?>

                </div>

            </div>
        </div>
    </div>
</div>


