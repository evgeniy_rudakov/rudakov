<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 12.07.17
 * Time: 22:48
 */

use app\models\Category;
use app\models\Content;
use yii\helpers\Html;

?>
<aside id="fh5co-aside" role="complementary" class="border">

    <h1 id="fh5co-logo"><a href="/">WEB-developer</a></h1>
    <nav id="fh5co-main-menu" role="navigation">
        <ul>
			<?php
			$menu  = Category::getMenu();
			$level = 0;
			foreach ($menu as $n => $item_menu)
			{
				$active_class = '';
				if ($page_url == $item_menu->url)
				{
					$active_class = 'fh5co-active';
				}
				if ($item_menu->lvl == $level)
					echo '</li>';
				else if ($item_menu->lvl > $level)
					echo '<ul>';
				else
				{
					echo '</li>';
					for ($i = $level - $item_menu->lvl; $i; $i--)
					{
						echo '</ul>';
						echo '</li>';
					}
				}
				echo '<li class="' . $active_class . '"><a href="/' . $item_menu->url . '"><span>' . $item_menu->name . '</span></a>';
				$level = $item_menu->lvl;
			}

			for ($i = $level; $i; $i--)
			{
				echo '</li>';
				echo '</ul>';

			} ?>


        </ul>
    </nav>
    <div class="fh5co-footer">
        <p>
            <small>
                <?=Html::a('rudakov.site','http://rudakov.site');?>
                <span> <?=Html::a('Skype:','skype:barsiksookamordiy');?> barsiksookamordiy</span>
                <span><?=Html::a('Email','mailto:evgeny@rudakov.site');?>: evgeny@rudakov.site</span>
                <span><?=Html::a('Email','mailto:dreammago@gmail.com');?>: dreammago@gmail.com</span>
                <span><?= Yii::powered() ?></span>
            </small>
        </p>
        <ul>
            <li><a href="https://vk.com/sookamordiy" target="_blank"><i class="icon-vk"></i></a></li>
            <li><a href="skype:barsiksookamordiy"><i class="icon-skype"></i></a></li>
            <li><a href="mailto:dreammago@gmail.com"><i class="icon-mail"></i></a></li>
        </ul>
    </div>


</aside>

