<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 29.07.17
 * Time: 16:25
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>


<body>
<div id="fh5co-page">
    <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
	<?= $this->context->renderPartial('/layouts/menu', ['page_url' => 'index']) ?>


    <div id="fh5co-main">
        <div class="fh5co-narrow-content animate-box" data-animate-effect="fadeInLeft">
            <div class="row">
                <h1>Мои контакты</h1>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2><?= Html::a('Skype:', 'skype:barsiksookamordiy'); ?> barsiksookamordiy </h2>
                </div>
                <div class="col-md-6">
                    <h2><?= Html::a('Email', 'mailto:evgeny@rudakov.site'); ?>: evgeny@rudakov.site </h2>
                </div>
                <div class="col-md-6">
                    <h2><?= Html::a('Gmail', 'mailto:dreammago@gmail.com'); ?>: dreammago@gmail.com </h2>
                </div>
                <div class="col-md-6">
                    <h2><?= Html::a('VK', 'https://vk.com/sookamordiy', ['target' => '_blank']); ?>: Евгений
                        Рудаков</h2>
                </div>
            </div>


            <div class="row">
                <h1>Написать мне</h1>
            </div>
            <div class="js-add-ajax-responce"></div>


			<?php
			$form = ActiveForm::begin([
				'options' => [
					'class' => 'contact-form'
				]
			]);
			?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
							<?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
							<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
							<?= $form->field($model, 'phone')->label(false)->widget(\yii\widgets\MaskedInput::className(), [
								'mask'          => '+7 (999) 999 99 99',
								'options'       => [
									'class'       => 'form-control',
									'placeholder' => 'Телефон',
								],
								'clientOptions' => [
									'clearIncomplete' => true
								]
							]); ?>
                        </div>
                        <div class="col-md-6">
							<?= $form->field($model, 'text')->textarea([
								'placeholder' => 'Сообщение',
								'cols'        => 30,
								'rows'        => 4
							])->label(false) ?>

                            <div class="form-group">
								<?= Html::button('Отправить', ['class' => 'btn btn-primary btn-md js-send-form-button']) ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
			<?php
			ActiveForm::end();
			?>

        </div>
    </div>
</div>
<?php
$send_form = <<<JS
$('#fh5co-page').on('click','.js-send-form-button', function() {
$('.js-send-form-button').prop('disabled',true);
$.ajax({
    url:'/site/send-email',
    method:'post',
    dataType: 'json',
    data:{
        Contact:{
            'name':$('#contact-name').val(),
            'email':$('#contact-email').val(),
            'phone':$('#contact-phone').val(),
            'text':$('#contact-text').val()
        }
    },
    success: function(data) {
        if (data.response=='send'){
            $('.col-md-6 input').val('');
            $('.col-md-6 textarea').val('');
            var html_message = '<div id="w1-success" style="display:none"  class="alert-success alert fade in">'+
                               '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                               '<i class="icon fa fa-check"></i>Спасибо за обращение!'+
            '</div>';
            $('.js-add-ajax-responce').html(html_message);
            $('#w1-success').show('slow');
            $('.js-send-form-button').prop('disabled',false);


            
        } else if (data.response=='not-send') {
            html_message = '<div style="display:none" id="w1-error" class="alert-danger alert fade in">'+
                               '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                               '<i class="icon fa fa-check"></i>Простите, с почтовым сервисом что-то пошло не так.'+
                               'Свяжитесь со мной иначе</div>';
            $('.js-add-ajax-responce').html(html_message);
            $('#w1-error').show('slow');
            $('.js-send-form-button').prop('disabled',false);

        } else  {
            html_message = '<div style="display:none" id="w1-warning" class="alert-warning alert fade in">'+
                               '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                               '<i class="icon fa fa-check"></i>Без введеных данных почтовый голубь не сможет доставить письмо.' +
                                '</div>';
            $('.js-add-ajax-responce').html(html_message);
            $('#w1-warning').show('slow');
            $('.js-send-form-button').prop('disabled',false);
        }
    }
  });
});
JS;
$this->registerJs($send_form, yii\web\View::POS_READY);


?>
