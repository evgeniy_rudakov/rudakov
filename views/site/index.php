<?php

/* @var $this yii\web\View */

$this->title = 'Rudakov.site';
use app\components\renderContentView;

?>

<div id="fh5co-page">
    <a href="/" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
    <?= $this->context->renderPartial('/layouts/menu',['page_url'=>'index']) ?>

    <div id="fh5co-main">
        <div class="main-page-content fh5co-narrow-content col-md-11 animate-box fadeInLeft animated">
        <?=$main_page->content?>

        </div>
    </div>
</div>