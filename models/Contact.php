<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $email
 * @property string $phone
 * @property integer $created_at
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['created_at'], 'integer'],
	        [['name','email'],'required'],
	        [['email'],'email'],
            [['name', 'email','phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Имя'),
            'text' => Yii::t('app', 'Сообщение'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Телефон'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ContactQuery(get_called_class());
    }

        public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject('Обращение с сайта rudakov.site')
                ->setTextBody(Html::decode('Имя:'.$this->name.' | Email:'.$this->email.' | Phone:'.$this->phone.' | Text:'.$this->text))
                ->send();
            return true;
        }
        return false;
    }
}
