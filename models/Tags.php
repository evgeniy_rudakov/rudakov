<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $text
 * @property integer $created_at
 *
 * @property ItemsTags[] $itemsTags
 */
class Tags extends \yii\db\ActiveRecord
{

    const BLOG_TAG_TYPE = 0;
    const CONTENT_TAG_TYPE = 1;
    const ARTICLE_TAG_TYPE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @param $item_id
     * @param $item_type
     * @param $string_tags
     */
    public static function updateTags($item_id, $item_type, $string_tags)
    {
        if ($string_tags != '') {
            $tags = explode(',', $string_tags);
            ItemsTags::deleteAll(['item_id' => $item_id, 'type' => $item_type]);
            foreach ($tags as $one_tag_text) {
                $tag = self::find()->where(['text' => $one_tag_text])->one();
                if (!isset($tag)) {
                    $tag = new Tags();
                    $tag->text = $one_tag_text;
                    $tag->created_at = time();
                    $tag->save();
                }
                $item_tag = new ItemsTags();
                $item_tag->item_id = $item_id;
                $item_tag->tag_id = $tag->id;
                $item_tag->created_at = time();
                $item_tag->type = $item_type;
                $item_tag->save();
            }
        }
    }

    public static function loadTags($item_id, $item_type)
    {
        $items_tags = ItemsTags::find()->where(['item_id' => $item_id, 'type' => $item_type])->all();
        $result_string = '';
        if (!empty($items_tags)) {
            foreach ($items_tags as $item_tag) {
                $result_string .= $item_tag->tag->text . ', ';
            }
        }
        $result_string = mb_substr($result_string, 0, -2);

        return $result_string;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsTags()
    {
        return $this->hasMany(ItemsTags::className(), ['tag_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TagsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\TagsQuery(get_called_class());
    }
}
