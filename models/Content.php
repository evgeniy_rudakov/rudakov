<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $content
 * @property string $cat
 * @property string $github_url
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $category_id
 *
 * @property Category $category
 * @property User $user
 * * @property ItemsTags[] $itemsTags
 */
class Content extends \yii\db\ActiveRecord
{
	public $categoryName;
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'cat'], 'string'],
            [['user_id', 'category_id'], 'required'],
	        [['categoryName'],'safe'],
            [['user_id', 'created_at', 'category_id'], 'integer'],
            [['title', 'url', 'github_url'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Url'),
            'content' => Yii::t('app', 'Content'),
            'cat' => Yii::t('app', 'Cat'),
            'github_url' => Yii::t('app', 'Github Url'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }

    public function getCategoryName(){
    	return $this->category->name;
    }

    /**
     * function for get url of img, or 'no-image' if img was not downloaded
     * @param $img_type - small, orig, mini
     * @param $folder_number
     * @return string
     */
    public function getImgUrl($img_type, $folder_number)
    {
        if (file_exists(Yii::getAlias('@webroot/img/content/' . $this->id . '/' . $folder_number . '/' . $img_type . '.jpg'))) {
            return '/img/content/' . $this->id . '/' . $folder_number . '/' . $img_type . '.jpg';
        } else {
            return '/img/no_image.png';
        }
    }

    public function getItemsTags()
    {
        return $this->hasMany(ItemsTags::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Method for upload image from _from
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            if ($this->imageFile) {
                $path = $_SERVER['DOCUMENT_ROOT'] . "/img/content/" . $this->id;
                if (!is_dir($path)) {
                    mkdir($path, 0755);
                }
                $folder_path = $path . '/1';
                if (!is_dir($folder_path))
                    mkdir($folder_path, 0755);
                $this->imageFile->saveAs($_SERVER['DOCUMENT_ROOT'] . '/img/content/' . $this->id . '/1/orig.jpg');
            }
            return true;
        } else {
            return false;
        }
    }

    public static function rusToLat($string)
    {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return strtolower(str_replace($rus, $lat, $string));
    }

    public static function latToRus($string)
    {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return strtolower(str_replace($lat, $rus, $string));
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ContentQuery(get_called_class());
    }
}
