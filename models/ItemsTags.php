<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items_tags".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $created_at
 * @property integer $type
 * @property integer $tag_id
 *
 * @property Content $item
 * @property Tags $tag
 */
class ItemsTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'tag_id'], 'required'],
            [['item_id', 'created_at', 'type', 'tag_id'], 'integer'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Content::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'type' => Yii::t('app', 'Type'),
            'tag_id' => Yii::t('app', 'Tag ID'),
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Content::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tag_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ItemsTagsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ItemsTagsQuery(get_called_class());
    }
}
