<?php

use yii\db\Migration;

class m170729_132937_add_contact_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170729_132937_add_contact_table cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
		$this->createTable('contact',[
			'id'=>'pk',
			'name'=>'string',
			'text'=>'text',
			'email'=>'string',
			'phone'=>'string',
			'created_at'=>'integer',
			]);
    }

    public function down()
    {
        $this->dropTable('contact');
        return false;
    }
}
