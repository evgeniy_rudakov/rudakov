<?php

use yii\db\Migration;

/**
 * Handles adding cat to table `content`.
 */
class m170723_131408_add_cat_column_to_content_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('content', 'cat', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('content', 'cat');
    }
}
