<?php

use yii\db\Migration;

/**
 * Handles adding url to table `category`.
 */
class m170729_113317_add_url_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	        $this->addColumn('category', 'url', $this->string());
    }

    /**
     * @inheritdoc
     */

    public function down()
    {
    	$this->dropColumn('category','url');
    }
}
