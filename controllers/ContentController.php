<?php
/**
 * Created by PhpStorm.
 * User: evg
 * Date: 12.07.17
 * Time: 23:00
 */

namespace app\controllers;

use app\models\Category;
use app\models\Content;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class ContentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays blog.
     *
     * @return string
     */
    public function actionIndex($page_url)
    {
        $category = Category::find()->where(['url' => $page_url])->one();
        return $this->render('index', [
            'category' => $category,
            'page_url' => $page_url,

        ]);
    }

    public function actionView($page_url, $id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'page_url' => $page_url,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
